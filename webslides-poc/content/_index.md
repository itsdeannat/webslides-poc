+++
title = "Using Git with Visual Studio Code"
+++
<!--: .wrap .size-70 ..aligncenter -->


## **Using Git with Visual Studio Code**

<img src="static/images/Visual_Studio_Code_1.35_icon.svg.png" width="300" height="350">


---

<!-- : .wrap .size-40 ..aligncenter -->

## **Agenda**

<br />

**Review essential Git concepts**
{ .text-uppercase }
Committing...branching...what does that mean again?

**What is Visual Studio Code** 
{ .text-uppercase }
And why should you use it to write docs?

**Demo: Help the Good Dogs Project**  
{ .text-uppercase }
Where are you located exactly?


---
<!--: .wrap .size-50 ..aligncenter -->

## Git terminology review

Committing...branching...what does that mean again?

---
<!--: .wrap .size-50 -->

|||v
## Clone
{ .fadeInUp }

When you clone a repository, you copy the repository to your local machine 
{ .fadeInUp }

<img class="aligncenter" src="static/images/clone.png" alt="Fork and clone">

---
<!--: .wrap .size-50 -->

|||v
## Fork and clone
{ .fadeInUp }

When you fork a repository, you create an independent copy of the repository that exists in GitLab under your namespace. Then you clone your fork to your local machine.
{ .fadeInUp }

<img class="aligncenter" src="static/images/fork-and-clone.png" alt="Clone">


---
<!-- : .wrap .size-50 -->

### Branch

An independent line of development 
{ .fadeInUp }


<img class="aligncenter" src="static/images/branch.png" width="650" alt="Git branches">


---

<!-- : .wrap .size-50 -->
### Commit

Saving your changes to your local repository
{ .fadeInUp }

<img class="aligncenter" src="static/images/commits.png" width="650" alt="Git branches">


---
<!-- : .wrap .size-50 -->
### Merge

Integrating changes from another branch
{ .fadeInUp }

<img class="aligncenter" src="static/images/merge.png" width="650" alt="Git branches">

---
<!--: .wrap .size-70 ..aligncenter -->

## **What is Visual Studio Code?**

<img src="static/images/Visual_Studio_Code_1.35_icon.svg.png" width="300" height="350">

---
<!-- : .wrap .size-50 -->
### Visual Studio Code is a lightweight but powerful code editor designed by Microsoft.

It's one of the most popular code editors out there for several reasons!
{ .fadeInUp }

---
<!-- : .wrap -->

|||v

### Code editing

With Visual Studio Code, you can:

- Develop web applications with ease
- Use VS Code's Intellisense feature to help you catch any syntax errors
{.description}

|||

### Text editing

You can also:

- Write and edit text files written in languages like Markdown, AsciiDoc, or plain .txt files
- Use VS Code's extensions for authoring assistance
{.description }

|||

### Version control

Git and VS Code go well together, too!

- Run command Git commands in the built-in, integrated terminal 
- Use VS Code extensions to power up your Git workflow, like GitLens
{.description}
